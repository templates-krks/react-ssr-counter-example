

import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import App from './app';

const render = (Root) => {
	ReactDOM.render(
		<AppContainer>
			<Root/>
		</AppContainer>,
		document.getElementById('app')
	);
};

render(App);

// Hot Module Replacement API
if (module.hot) {
	module.hot.accept('./app', () => {
		const NewApp = require('./app').default;
		render(NewApp)
	});
}

