

import React from 'react';
import ReactDOM from 'react-dom';
import Routes from '../shared/routes';
import {Router, browserHistory} from 'react-router';
import {Provider} from 'react-redux';
import configureStore from '../shared/store';


let store = configureStore(window.__PRELOADED_STATE__);

ReactDOM.render(
    <Provider store={store}>
        <Router routes={Routes}
                history={browserHistory}
        />
    </Provider>
    , document.getElementById('app')
);