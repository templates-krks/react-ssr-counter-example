
import React from 'react';
import Routes from '../shared/routes';
import {Router, browserHistory} from 'react-router';
import {Provider} from 'react-redux';
import configureStore from '../shared/store';

let store = configureStore(window.__PRELOADED_STATE__);

const App = function(){
    return (
	<Provider store={store}>
	  <Router routes={Routes}
		  history={browserHistory}
		  />
	</Provider>
    ); 
}

export default App;
