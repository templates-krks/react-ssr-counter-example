


export default () => {

    return new Promise((resolve, reject) => {
        const data = {
            someContent: 'This is some content passed from the server',
            someNumber: 124324
        };
        setTimeout(() => resolve(data), 500);
    });

}