
import webpackMiddleware from 'webpack-dev-middleware';
import webpack from 'webpack';
import config from '../../../webpack/dev.config.js';
import webpackHotMiddleware from 'webpack-hot-middleware';

const options = {
    hot: true,
    inline: true,
    lazy: false,
    publicPath: config.output.publicPath,
    stats: {
	colors: true,
	chunks: false,
    },
    serverSideRender: true,
};


export default function(){
    const compiler = webpack(config);
    const devMiddleware = webpackMiddleware(compiler); 
    const hotMiddleware = webpackHotMiddleware(compiler);
    return {
	devMiddleware,
	hotMiddleware
    };
}
