
export default function (locals){

    const {styleSheet, scriptBundle, error, html, preloadedState} = locals;

    const errorHTML =
            `<script>
                 console.log('');
                 console.error('####################################################');
                 console.error('SSR ERROR');
                 console.error('####################################################');
                 console.log('');
             </script>`
        ;

    return `
            <html>
                <head>
                 
                    ${styleSheet ? `<link rel="stylesheet" href=${styleSheet}>` : ""}
                    <script id="remove">
                        window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)};
                        // for declaring global variables based on template locals
                        var script = document.getElementById('remove');
                        var head = document.getElementsByTagName('head')[0];
                        head.removeChild(script);
                    </script>
                    
                </head>
                
                <body>
                        
                        <div id="app">${error ? errorHTML : html}</div>
                        <script src=${scriptBundle}></script>
                </body>
                
                
            </html>
    `


}