

import { match, RouterContext } from 'react-router';
import routes from '../../shared/routes';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import path from 'path';
import {Provider} from 'react-redux';
import configureStore from '../../shared/store';
import getContent from '../db/';
import renderTemplate from './template';

export default  (req, res) => {

    match({routes, location: req.url}, function(error, redirectLocation, renderProps) {

        if (error) {

            console.log('Error', error);
            res.status(500).send(error);

        } else if(redirectLocation){

            res.redirect(302, redirectLocation.pathname + redirectLocation.search)

        } else if(renderProps){

            let locals = {};

            getContent()
                .then(content =>{

                    const preloadedState =  {
                        content: content,
                        counter: {count: 123},
                    };

                    const store = configureStore(preloadedState);

                    try{
                        locals.html = ReactDOMServer.renderToString(
                            <Provider store={store}>
                                <RouterContext {...renderProps}/>
                            </Provider>
                        );
                    } catch (e){
                        locals.SSR_ERROR = true;
                        console.log(e.stack);
                    }


                    if(process.env.NODE_ENV === 'development'){
                        let config = require('../../../webpack/dev.config');
                        //locals.scriptBundle = `${config.output.publicPath}/${config.output.filename}`;
                        locals.scriptBundle = `/${config.output.filename}`;
                    } else {
                        let assets = require( path.join(appRoot, '/dist/assets/stats.json') );
                        locals.scriptBundle = `/${assets.main}`;
                        locals.styleSheet = `/${assets.css}`;
                    }

                    locals.preloadedState = store.getState();
                    res.send( renderTemplate(locals) );

                });




        } else {

            res.status(404).send('Not found');

        }
    });
}
