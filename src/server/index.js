




import path from 'path';
import Express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import handleRender from './ssr';
import createDevMiddlewares from './middleware/webpack-dev-middlewares.js';



const NODE_ENV = process.env.NODE_ENV;
const HOST = process.env.HOST || '0.0.0.0';
const PORT = process.env.PORT;

if(!PORT){
    throw new Error('no PORT specified in environment');
}

export default () => {
    
    const app = Express();
    if(NODE_ENV === 'development'){
	const {devMiddleware, hotMiddleware} = createDevMiddlewares();
	app.use(devMiddleware);
	app.use(hotMiddleware);
    }
    app.use(bodyParser.json());
    app.use(compression());
    app.use( Express.static( path.join(appRoot, 'dist/assets') ));
    app.use(handleRender);

    app.listen(PORT, HOST, () => {
	console.log(`server is listening at http://${HOST}:${PORT}`);
    });
}



