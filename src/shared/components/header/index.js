

import React from 'react';
import { Link } from 'react-router';
import styled from 'styled-components';


export const headerHeight = '10vh';

const Header = styled.div`
    border-bottom: 1px solid black;
    background-color: lightgray;
    color: white;
    width: 100%;
    height: ${headerHeight};
    display: flex;
    justify-content: space-around;
    align-items: center;
`;

export default props => {
    
    
    return (
        <Header>
            <Link to="/">Index</Link>
            <Link to="/counter">Counter</Link>
            <Link to="/one">View One</Link>
            <Link to="/two">View Two</Link>
        </Header>
    );
}