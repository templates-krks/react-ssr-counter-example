

import React from 'react';
import styled from 'styled-components';


const Counter = styled.div`
    width: 200px;
    height: 50%;
    color: black;
    display: flex;
    flex-flow: column wrap;
    align-items: center;
    justify-content: center;
`;

const Count = styled.div`
        font-size: 1.5em;
        font-weight: bold;
`;

export default props => {


    let {increment, decrement, count} = props;
    return (
        <Counter>
            <Count>Count: {count}</Count>
            <div>
                <button onClick={ decrement }>-</button>
                <button onClick={ increment }>+</button>
            </div>
        </Counter>
    )

}
