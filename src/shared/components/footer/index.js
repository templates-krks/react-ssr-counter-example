

import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';


export const footerHeight = '5vh';
const Container = styled.div`
    border-top: 1px solid black;
    background-color: lightgray;
    color: white;
    width: 100%;
    height: ${footerHeight};
    display: flex;
    justify-content: space-around;
    align-items: center;
`;

const Footer =  props => {

    return (
        <Container>
            Count: {props.count}
        </Container>
    );
};


export default connect(state => ({
    count: state.counter.count
}))(Footer);