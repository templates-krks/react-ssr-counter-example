


import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './app';
import IndexView from './views/index';
import CounterView from './views/counter';
import ViewOne from './views/one';
import ViewTwo from './views/two';
import DefaultLayout from './layouts/default';



export default

    <Route path="/" component={App}>
        <Route component={DefaultLayout}>
            <IndexRoute component={IndexView}/>
            <Route path="/one" component={ViewOne}/>
            <Route path="/two" component={ViewTwo}/>
            <Route path="/counter" component={CounterView}/>
        </Route>
    </Route>

;
