



import React from 'react';
import Loading from '../views/loading';

if(process.env.IS_BROWSER){
    require('../global-style/reset.css');
    require('../global-style/style.css');
}

import {connect} from 'react-redux';


class App extends React.Component {
    render () {
        if(!this.props.rehydrated){
            return <Loading/>;
        }
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

export default connect(state => ({
    rehydrated: state.rehydrated
}))(App);



