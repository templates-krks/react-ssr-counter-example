

import React from 'react';
import Header, {headerHeight} from '../../components/header';
import Footer, {footerHeight} from '../../components/footer';
import styled from 'styled-components';



const Layout = styled.div`
    width: 100vw;
    padding-left: 10vw;
    padding-right: 10vw;
    min-height: 100vh;
`;

const Body = styled.div`
    width: 100%;
    height: 100vh - ${headerHeight} - ${footerHeight};
`;



export default props => {


    return (
        <Layout>
            <Header/>
            <Body>
                {props.children}
            </Body>
            <Footer/>
        </Layout>
    );


};

