

import React from 'react';
import ViewContainer from '../../components/view-container';
import styled from 'styled-components';


const Container = styled(ViewContainer)`
    background-color: red;
`;

const ViewTwo = () => {
    return  (
        <Container>
	  View Two
	</Container>
    );
};

export default ViewTwo;
