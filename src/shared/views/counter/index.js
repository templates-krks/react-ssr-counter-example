
import React from 'react';
import Counter from '../../components/counter';
import {connect} from 'react-redux';
import {increment, decrement} from '../../store/actions/counter';
import ViewContainer from '../../components/view-container';
import styled from 'styled-components';

const Container = styled(ViewContainer)`
    background-color: blueviolet;
`;

const CounterView = props => {

    let {count, dispatch} = props;

    return  (
        <Container>
            <Counter count={count}
                     increment={() => dispatch(increment())}
                     decrement={() => dispatch(decrement())}
            />
        </Container>
    )
};


export default connect(state => ({
    count: state.counter.count
}))(CounterView);