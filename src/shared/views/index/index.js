
import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ViewContainer from '../../components/view-container';

const Container = styled(ViewContainer)`
    background-color: black;
    color: white;
`;

const IndexView = props => {
    const someNumber = props.content.someNumber;
    return  (
        <Container>
            INDEX <br/>
            {props.content.someContent} <br/>
            Some Data: {someNumber}
        </Container>
    );
};

export default connect(state => ({
    content: state.content
}))(IndexView);