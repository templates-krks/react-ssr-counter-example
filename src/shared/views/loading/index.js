

import React from 'react';
import ViewContainer from '../../components/view-container';
import styled from 'styled-components';


const Container = styled(ViewContainer)`
    background-color: #ffff99;
    font-size: large;
    font-weight: bold;
`;

const Loading  = props => {
    return (
        <Container>
            Loading...
        </Container>
    )
};

export default Loading;