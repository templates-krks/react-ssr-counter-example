




import React from 'react';
import ViewContainer from '../../components/view-container';
import styled from 'styled-components';


const Container = styled(ViewContainer)`
    background-color: mediumseagreen;
`;

const ViewOne = () => {
    
    
    return  (
        
        <Container>View One</Container>
        
    )
    
};

export default ViewOne;