

import {ActionTypes} from '../actions/counter';



// will be overwritten by preloaded state passed from server
let initialState = {count: -1};

export default (state = initialState, action) => {

    switch (action.type){

        case ActionTypes.INCREMENT:{

            return {
                ...state,
                count: state.count + 1
            }

        }

        case ActionTypes.DECREMENT:{
            return {
                ...state,
                count: state.count - 1
            }
        }

        default:
            return state

    }

}