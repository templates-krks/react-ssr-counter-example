

const ActionTypes = {

    INCREMENT: "counter/INCREMENT",
    DECREMENT: "counter/DECREMENT"

};

export {ActionTypes};




export function increment(){

    return {
        type: ActionTypes.INCREMENT
    }

}


export function decrement(){
    return {
        type: ActionTypes.DECREMENT
    }
}
