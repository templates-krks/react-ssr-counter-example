
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const StatsWriterPlugin = require("webpack-stats-plugin").StatsWriterPlugin;

module.exports = {
    devtool: false,
    entry: {
	main: './src/client/index.js',
    },
    output: {
	path: path.resolve('dist/assets'),
	filename: '[hash].min.js',
    },
    module:{
	rules:[
	    {
		test: /\.(png|woff|woff2|eot|ttf|svg)$/,
		loader: 'url-loader?limit=100000'
	    },
	    {
		test: /\.jsx?$/,
		loader: 'babel-loader',
		include: [ path.resolve('./src/client/'), path.resolve('src/shared/') ]
	    },
	    {
		test: /\.css$/,
		use: ExtractTextPlugin.extract({
		    fallback: 'style-loader',
		    use: [
			'css-loader',
			'postcss-loader'
		    ]
		})
	    },
	],
    },
    plugins: [
	new ExtractTextPlugin("[hash].min.css"),
	// Write out stats file to build directory.
	new StatsWriterPlugin({
	    transform: function (data) {
		return JSON.stringify({
		    main: data.assetsByChunkName.main[0],
		    css: data.assetsByChunkName.main[1]
		}, null, 4);
	    }
	}),
    ]
};
