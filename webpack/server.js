
import WebpackDevServer from "webpack-dev-server";
import webpack from "webpack";
import config from "./dev.config";

const host = process.env.HOST || "192.168.1.59";
const port = process.env.WP_DEV_PORT || 3001;

const options = {
    hot: true,
    inline: true,
    lazy: false,
    publicPath: config.output.publicPath,
    stats: {
	colors: true,
	chunks: false,
    }
};

const compiler = webpack(config);
const webpackDevServer = new WebpackDevServer(compiler, options);

webpackDevServer.listen(port, host, function() {
    console.log(`Webpack development server listening on ${options.contentBase}`);
});
