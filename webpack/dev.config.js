
const path = require('path');
const webpack = require('webpack');
const dist = path.resolve(__dirname, '../dist');
//const DashboardPlugin = require('webpack-dashboard/plugin');

const {HOST, PORT} = process.env;

module.exports = {
    context: path.resolve(__dirname, '../src/client'),
    devtool: "source-map",
    entry: [
	'react-hot-loader/patch',
	"webpack-hot-middleware/client?reload=false",
	'./index.js'
    ],
    output: {
	path: dist,
	filename: 'bundle.js',
	publicPath: '/',
    },
    module:{
	rules:[
	    {
		test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/,
		use: [
		    'url-loader'
		]
	    },
	    {
		test: /\.css$/,
		use: [
		    'style-loader',
		    'css-loader'
		]
	    },
	    {
		test: /\.jsx?$/,
		use: [
		    'babel-loader'
		],
		include: [
		    path.resolve(__dirname, '../src/client/'),
		    path.resolve(__dirname, '../src/shared/')
		]
	    }
	]
    },
    
    plugins: [
	//new DashboardPlugin(),
	new webpack.HotModuleReplacementPlugin(),
	new webpack.NamedModulesPlugin(),
	new webpack.DefinePlugin({
	    'process.env': {
		'IS_BROWSER': "'true'",
		'NODE_ENV': "'development'"
	    }
	})
    ]

};
