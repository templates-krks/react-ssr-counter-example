

const path = require('path');

global.appRoot = path.resolve(__dirname);

if(process.env.NODE_ENV === 'development'){
    require('dotenv').load();
    require('babel-register');
    require('./src/server').default();
}

if(process.env.NODE_ENV === 'production'){
    require('./dist/server').default();
}
